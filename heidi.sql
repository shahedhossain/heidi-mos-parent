-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 07, 2014 at 08:58 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heidi`
--

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE IF NOT EXISTS `authorities` (
  `f_authority_id` int(11) NOT NULL,
  `f_authority` varchar(60) NOT NULL,
  `f_note` varchar(100) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_authority_id`),
  UNIQUE KEY `f_authority` (`f_authority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`f_authority_id`, `f_authority`, `f_note`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'ROLE_COMPANY_CREATE', 'Single record insert permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'ROLE_COMPANY_READ', 'Single record read permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'ROLE_COMPANY_UPDATE', 'Single record update permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'ROLE_COMPANY_DELETE', 'Single record delete permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'ROLE_COMPANY_BATCH_CREATE', 'Multi record insert permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(6, 'ROLE_COMPANY_BATCH_READ', 'Multi record read permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(7, 'ROLE_COMPANY_BATCH_UPDATE', 'Multi record update permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(8, 'ROLE_COMPANY_BATCH_DELETE', 'Multi record delete permission on Company', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(9, 'ROLE_CITY_CREATE', 'Single record insert permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(10, 'ROLE_CITY_READ', 'Single record read permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(11, 'ROLE_CITY_UPDATE', 'Single record update permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(12, 'ROLE_CITY_DELETE', 'Single record delete permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(13, 'ROLE_CITY_BATCH_CREATE', 'Multi record insert permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(14, 'ROLE_CITY_BATCH_READ', 'Multi record read permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(15, 'ROLE_CITY_BATCH_UPDATE', 'Multi record update permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(16, 'ROLE_CITY_BATCH_DELETE', 'Multi record delete permission on City', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(17, 'ROLE_OFFICE_CREATE', 'Single record insert permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(18, 'ROLE_OFFICE_READ', 'Single record read permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(19, 'ROLE_OFFICE_UPDATE', 'Single record update permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(20, 'ROLE_OFFICE_DELETE', 'Single record delete permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(21, 'ROLE_OFFICE_BATCH_CREATE', 'Multi record insert permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(22, 'ROLE_OFFICE_BATCH_READ', 'Multi record read permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(23, 'ROLE_OFFICE_BATCH_UPDATE', 'Multi record update permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(24, 'ROLE_OFFICE_BATCH_DELETE', 'Multi record delete permission on Office', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(25, 'ROLE_DEPARTMENT_CREATE', 'Single record insert permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(26, 'ROLE_DEPARTMENT_READ', 'Single record read permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(27, 'ROLE_DEPARTMENT_UPDATE', 'Single record update permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(28, 'ROLE_DEPARTMENT_DELETE', 'Single record delete permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(29, 'ROLE_DEPARTMENT_BATCH_CREATE', 'Multi record insert permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(30, 'ROLE_DEPARTMENT_BATCH_READ', 'Multi record read permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(31, 'ROLE_DEPARTMENT_BATCH_UPDATE', 'Multi record update permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(32, 'ROLE_DEPARTMENT_BATCH_DELETE', 'Multi record delete permission on Department', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(33, 'ROLE_WORKER_CREATE', 'Single record insert permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(34, 'ROLE_WORKER_READ', 'Single record read permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(35, 'ROLE_WORKER_UPDATE', 'Single record update permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(36, 'ROLE_WORKER_DELETE', 'Single record delete permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(37, 'ROLE_WORKER_BATCH_CREATE', 'Multi record insert permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(38, 'ROLE_WORKER_BATCH_READ', 'Multi record read permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(39, 'ROLE_WORKER_BATCH_UPDATE', 'Multi record update permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(40, 'ROLE_WORKER_BATCH_DELETE', 'Multi record delete permission on Worker', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `authorities_roles`
--

CREATE TABLE IF NOT EXISTS `authorities_roles` (
  `k_authority_id` int(11) NOT NULL,
  `k_role_id` int(11) NOT NULL,
  UNIQUE KEY `k_authority_id` (`k_authority_id`,`k_role_id`),
  KEY `fk_authorities_roles_role_id` (`k_role_id`),
  KEY `fk_authorities_roles_authority_id` (`k_authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authorities_roles`
--

INSERT INTO `authorities_roles` (`k_authority_id`, `k_role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(6, 4),
(9, 4),
(10, 4),
(11, 4),
(12, 4),
(14, 4),
(17, 4),
(18, 4),
(19, 4),
(20, 4),
(22, 4),
(25, 4),
(26, 4),
(27, 4),
(28, 4),
(30, 4),
(33, 4),
(34, 4),
(35, 4),
(36, 4),
(38, 4),
(1, 5),
(2, 5),
(3, 5),
(6, 5),
(9, 5),
(10, 5),
(11, 5),
(14, 5),
(17, 5),
(18, 5),
(19, 5),
(22, 5),
(25, 5),
(26, 5),
(27, 5),
(30, 5),
(33, 5),
(34, 5),
(35, 5),
(38, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `f_city_id` int(11) NOT NULL,
  `f_name` varchar(60) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`f_city_id`, `f_name`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'New York', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'Los Angeles', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'Chicago', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'New York', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'Los Angeles', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(6, 'Houston', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(7, 'Philadelphia', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(8, 'Phoenix', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(9, 'San Antonio', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(10, 'San Diego', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `f_company_id` int(11) NOT NULL,
  `f_name` varchar(60) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`f_company_id`, `f_name`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'Ecomputer Oy', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `f_department_id` int(11) NOT NULL,
  `f_name` varchar(60) NOT NULL,
  `k_company_id` int(11) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_department_id`),
  KEY `fk_departments_company_id` (`k_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`f_department_id`, `f_name`, `k_company_id`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'Admin', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'HRD', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'RND', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'AC', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'MRD', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(6, 'RTA', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(7, 'ICT', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(8, 'RNP', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(9, 'CRM', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments_workers`
--

CREATE TABLE IF NOT EXISTS `departments_workers` (
  `k_department_id` int(11) NOT NULL,
  `k_worker_id` int(11) NOT NULL,
  PRIMARY KEY (`k_department_id`,`k_worker_id`),
  KEY `fk_departments_workers_worker_id` (`k_worker_id`),
  KEY `fk_departments_workers_department_id` (`k_department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE IF NOT EXISTS `offices` (
  `f_office_id` int(11) NOT NULL,
  `f_name` varchar(60) NOT NULL,
  `k_company_id` int(11) NOT NULL,
  `k_city_id` int(11) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_office_id`),
  KEY `offices_company_id` (`k_company_id`),
  KEY `fk_offices_company_id` (`k_company_id`),
  KEY `fk_offices_city_id` (`k_city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`f_office_id`, `f_name`, `k_company_id`, `k_city_id`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'New York', 1, 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'Los Angeles', 1, 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'Chicago', 1, 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'New York', 1, 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'Los Angeles', 1, 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(6, 'Houston', 1, 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(7, 'Philadelphia', 1, 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(8, 'Phoenix', 1, 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(9, 'San Antonio', 1, 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(10, 'San Diego', 1, 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `request_maps`
--

CREATE TABLE IF NOT EXISTS `request_maps` (
  `f_requestmap_id` int(11) NOT NULL,
  `f_requestmap_url` varchar(100) NOT NULL,
  `f_config_attribute` varchar(60) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_requestmap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `f_role_id` int(11) NOT NULL,
  `f_role` varchar(60) NOT NULL,
  `f_note` varchar(100) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_role_id`),
  UNIQUE KEY `f_role` (`f_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`f_role_id`, `f_role`, `f_note`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'system', 'System User', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'admin', 'Admin User', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'manager', 'Manager User', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'hr', 'HR Manager User', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'user', 'General User', 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sequences`
--

CREATE TABLE IF NOT EXISTS `sequences` (
  `f_name` varchar(60) NOT NULL,
  `f_value` int(11) NOT NULL,
  PRIMARY KEY (`f_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sequences`
--

INSERT INTO `sequences` (`f_name`, `f_value`) VALUES
('authorities', 41),
('cities', 11),
('companies', 2),
('departments', 10),
('offices', 11),
('request_maps', 1),
('roles', 6),
('users', 5),
('workers', 231);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `f_user_id` int(11) NOT NULL,
  `f_username` varchar(60) NOT NULL,
  `f_password` varchar(100) NOT NULL,
  `f_account_active` tinyint(1) NOT NULL,
  `f_account_unlocked` tinyint(1) NOT NULL,
  `f_account_unexpired` tinyint(1) NOT NULL,
  `f_password_unexpired` tinyint(1) NOT NULL,
  `f_account_unlock_date` date NOT NULL,
  `f_account_expire_date` date NOT NULL,
  `f_password_expire_date` date NOT NULL,
  `k_role_id` int(11) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_user_id`),
  UNIQUE KEY `f_username` (`f_username`),
  KEY `fk_users_role_id` (`k_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`f_user_id`, `f_username`, `f_password`, `f_account_active`, `f_account_unlocked`, `f_account_unexpired`, `f_password_unexpired`, `f_account_unlock_date`, `f_account_expire_date`, `f_password_expire_date`, `k_role_id`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'system', 'system', 1, 1, 1, 1, '2014-01-01', '2024-12-31', '2024-12-31', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'admin', 'admin', 1, 1, 1, 1, '2014-01-01', '2024-12-31', '2024-12-31', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'manager', 'manager', 1, 1, 1, 1, '2014-01-01', '2024-12-31', '2024-12-31', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'hr', 'hr', 1, 1, 1, 1, '2014-01-01', '2024-12-31', '2024-12-31', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'user', 'user', 1, 1, 1, 1, '2014-01-01', '2024-12-31', '2024-12-31', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE IF NOT EXISTS `workers` (
  `f_worker_id` int(11) NOT NULL,
  `f_name` varchar(60) NOT NULL,
  `k_office_id` int(11) NOT NULL,
  `k_register_user` int(11) DEFAULT NULL,
  `f_register_date` datetime DEFAULT NULL,
  `k_revision_user` int(11) DEFAULT NULL,
  `f_revision_date` datetime DEFAULT NULL,
  `f_revision` int(11) NOT NULL,
  PRIMARY KEY (`f_worker_id`),
  KEY `fk_workers_office_id` (`k_office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`f_worker_id`, `f_name`, `k_office_id`, `k_register_user`, `f_register_date`, `k_revision_user`, `f_revision_date`, `f_revision`) VALUES
(1, 'A K M  Rokunuzzaman', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(2, 'A K M Jafor Ikbal Khan', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(3, 'Abdul  Alim', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(4, 'Abdul  Aoual', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(5, 'Abdul  Halim', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(6, 'Abdul  Hannan', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(7, 'Abdul  Haque', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(8, 'Abdul  High', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(9, 'Abdul  Kaium', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(10, 'Abdul  Kayum', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(11, 'Abdul Alim  Molla', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(12, 'Abdul Malek  Khan', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(13, 'Abdul Mannan Khan', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(14, 'Abdur  Rajjak', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(15, 'Abdur  Rashid', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(16, 'Abdur  Razzak', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(17, 'Abdur  Roaf', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(18, 'Abdus  Salam', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(19, 'Abdus Salam Fakir', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(20, 'Abdus Samad Chowdhury', 1, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(21, 'Abu  Musa', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(22, 'Abu  Taleb', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(23, 'Abu Ahmed Faroque', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(24, 'Abu Ali Siddique', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(25, 'Abu Sayed Salim', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(26, 'Abu Soeb Miah', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(27, 'Abul  Basar', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(28, 'Abul  Hossain', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(29, 'Abul  Kashem', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(30, 'Abul Kalam Azad', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(31, 'Abul Nur Tushar', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(32, 'Ahsan  Habib', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(33, 'Alamgir  Hossen', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(34, 'Ali  Ahammad', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(35, 'Ali  Zinnah', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(36, 'AMANA MCS LTD', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(37, 'Amena  Amena', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(38, 'Amir  Hossain', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(39, 'Anisur  Rahman', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(40, 'Ashiqur  Rahman', 2, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(41, 'Asma  Akter', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(42, 'Asma  Khandoker', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(43, 'Atkia Samia Tabassum', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(44, 'Awal  Mia', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(45, 'Ayesha  Khatun', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(46, 'Aynal Hossain Sorkar', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(47, 'Basori  Begum', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(48, 'Basri  Begum', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(49, 'Beauty Akter Beauty', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(50, 'Biswajit  Voumic', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(51, 'Dalim  Miah', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(52, 'Deloar  Hossain', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(53, 'Dulal  Mia', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(54, 'Enamul Haque Rayhan', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(55, 'Eusuf  Eusuf', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(56, 'Fardin Ehsan Sabbir', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(57, 'Farid  Uddin', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(58, 'Farida  Bari', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(59, 'Fatema  Alom', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(60, 'Fatima  Hossain', 3, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(61, 'Fazle  Rabby', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(62, 'Fazlul Haq Uzzol', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(63, 'Golam  Mostafa', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(64, 'Golam Mahmud Mamun', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(65, 'H M Maksudur Rahman', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(66, 'Habibur  Rahman', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(67, 'Hasan  Imam', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(68, 'Hayat Mahmud  Sabbir', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(69, 'Humayun  Kabir', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(70, 'Imran  Hossain', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(71, 'Irani  Khanom', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(72, 'Ismail  Hossen', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(73, 'Ismail  Khan', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(74, 'Ismat Jahan Lovely', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(75, 'Jafreen  Parveen', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(76, 'Jahangir  Alam', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(77, 'Jahangir  Alom', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(78, 'Jahangir  Hossain', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(79, 'Jahid  Hasan', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(80, 'Jakir Hossain A B M Azizur Rahman', 4, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(81, 'Jannatul  Ferdaws', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(82, 'Jannatur Rayhan  Chowdhury', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(83, 'Jasim  Uddin', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(84, 'Jayeda  Begum', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(85, 'Jemi  Jemi', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(86, 'Jerin  Tasnim', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(87, 'Jharna  Akter', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(88, 'Jinia  Afrin', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(89, 'Josna  Akter', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(90, 'Joynal  Abedin', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(91, 'Julfikar Ali Vutto', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(92, 'K H M Tajul  Islam', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(93, 'Kamal  Hossain', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(94, 'Kamal  Uddin', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(95, 'Kamrul  Hosan', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(96, 'Kazi A K M  Iqbal', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(97, 'Khadija  Akter', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(98, 'Khan Liakat  Ali', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(99, 'Kohinur  Akter', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(100, 'Lokman  Hossen', 5, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(101, 'Lulu  Marjan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(102, 'M Liakat Ali Khan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(103, 'M Mahiuddin  Islam', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(104, 'M Mohiuddin  Islam', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(105, 'Mabinul  Akher', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(106, 'Mahbubur  Rahman', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(107, 'Mahbubur Rahman Sowrav', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(108, 'Mamun  Talukder', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(109, 'Mamunur  Rashid', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(110, 'Mani  Akter', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(111, 'Marium  Begum', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(112, 'Marium  Marium', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(113, 'Masuma  Khatun', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(114, 'Md  Kamruzzaman', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(115, 'Md  Manik', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(116, 'Md  Mostafa', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(117, 'Md  Sentu', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(118, 'Md  Shahjahan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(119, 'Md  Shahjalal', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(120, 'Md  Yousup', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(121, 'Md Abdul  Bari', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(122, 'Md Abdul  Mannan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(123, 'Md Abdul Aziz', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(124, 'Md Abdul Halim Bhuiyan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(125, 'Md Abdul High', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(126, 'Md Abdul Mannaf Khan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(127, 'Md Abdur  Rajjak', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(128, 'Md Abdus  Salam', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(129, 'Md Abu  Rayhan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(130, 'Md Abu Bakar  Siddique', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(131, 'Md Abu Rayhan', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(132, 'Md Abul Hasem', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(133, 'Md Abul Kalam  Azad', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(134, 'Md Abur  Rajjak', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(135, 'Md Ahsan  Habib', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(136, 'Md Ahsan  Ullah', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(137, 'Md Al Amin Sarkar', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(138, 'Md Al Amin', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(139, 'Md Alamgir Hossen', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(140, 'Md Ali  Akbar', 6, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(141, 'Md Ali Jinnah', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(142, 'Md Alomgir  Hossain', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(143, 'Md Altaf  Ali', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(144, 'Md Amin  Ullah', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(145, 'Md Amir Hossain', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(146, 'Md Anowar Hossain', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(147, 'Md Anwar Hossain', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(148, 'Md Anwar Hossen', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(149, 'Md Asraf  Hossain', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(150, 'Md Ayubur Rahman', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(151, 'Md Azahar Shikder', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(152, 'Md Azizul  Haque', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(153, 'Md Borhan Uddin', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(154, 'Md Chan Kha', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(155, 'Md Emdadul Haq', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(156, 'Md Farid Uddin', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(157, 'Md Faruk  Hossain', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(158, 'Md Faruk Bhuiyan', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(159, 'Md Golam Zakaria', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(160, 'Md Hanif Patwary', 7, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(161, 'Md Ikbal  Hossain', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(162, 'Md Iqbal  Hossain', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(163, 'Md Jahangir  Hossain', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(164, 'Md Jahedi Hasan', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(165, 'Md Jahidul  Alom', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(166, 'Md Jakiul Islam', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(167, 'Md Jamsed  Ali', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(168, 'Md Jasim  Uddin', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(169, 'Md Jiaur Rahman', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(170, 'Md Joynal  Abedin', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(171, 'Md Kamal  Uddin', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(172, 'Md Kamal Uddin', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(173, 'Md Kamrul  Hasan', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(174, 'Md Kibria Khalashi', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(175, 'Md Liton  Bapari', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(176, 'Md Lutfar  Rahman', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(177, 'Md Madud  Khalashi', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(178, 'Md Mahbubur Rahman', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(179, 'Md Mamunur Rashid', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(180, 'Md Masud  Rana', 8, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(181, 'Md Maynul Islam', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(182, 'Md Mijanur Rahman', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(183, 'Md Minjurul Islam', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(184, 'Md Mintu Khan', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(185, 'Md Mir  Hossain', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(186, 'Md Mojahar  Ali', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(187, 'Md Mosharaf  Hossain', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(188, 'Md Mosharaf Hossain', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(189, 'Md Mostafizur Rahman', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(190, 'Md Murad  Khan', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(191, 'Md Nafiur  Rahman', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(192, 'Md Najir  Ahmed', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(193, 'Md Nure  Alom', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(194, 'Md Nurul Islam', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(195, 'Md Omar  Faruk', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(196, 'Md Rabiul  Alom', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(197, 'Md Rabiul  Islam', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(198, 'Md Rabiul Alom', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(199, 'Md Rabiul Islam', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(200, 'Md Rafiqul Islam', 9, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(201, 'Md Rakibul  Islam Khan', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(202, 'Md Rakibul  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(203, 'Md Rowsan Ali  Chowdhury', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(204, 'Md Sabbir  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(205, 'Md Sadib  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(206, 'Md Saiful  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(207, 'Md Saiful Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(208, 'Md Salah  Uddin', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(209, 'Md Samsul  Alom', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(210, 'Md Shafiqul  Auwal', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(211, 'Md Shafiqul  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(212, 'Md Shafiur Rahman', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(213, 'Md Shah  Alom', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(214, 'Md Shahadut Hossain', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(215, 'Md Shahed Hossain', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(216, 'Md Shahidul  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(217, 'Md Shahidul Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(218, 'Md Shahidullah Habil', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(219, 'Md Shahin Sarker', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(220, 'Md Shahjahan Shiraj', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(221, 'Md Shamim Islam Babu', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(222, 'Md Shohagh Hossain', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(223, 'Md Shohanul  Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(224, 'Md Shohidul Islam', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(225, 'Md Sohel  Rana', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(226, 'Md Sowkat  Ali', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(227, 'Md Sumon Bappi', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(228, 'Md Sydur  Rahman', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(229, 'Md Tariqul  Islam Khan', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1),
(230, 'Md Tawlad Hossain', 10, 1, '2014-01-01 00:00:00', 1, '2014-01-01 00:00:00', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authorities_roles`
--
ALTER TABLE `authorities_roles`
  ADD CONSTRAINT `fk_authorities_roles_authority_id` FOREIGN KEY (`k_authority_id`) REFERENCES `authorities` (`f_authority_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_authorities_roles_role_id` FOREIGN KEY (`k_role_id`) REFERENCES `roles` (`f_role_id`) ON UPDATE CASCADE;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `fk_departments_company_id` FOREIGN KEY (`k_company_id`) REFERENCES `companies` (`f_company_id`) ON UPDATE CASCADE;

--
-- Constraints for table `departments_workers`
--
ALTER TABLE `departments_workers`
  ADD CONSTRAINT `fk_departments_workers_department_id` FOREIGN KEY (`k_department_id`) REFERENCES `departments` (`f_department_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_departments_workers_worker_id` FOREIGN KEY (`k_worker_id`) REFERENCES `workers` (`f_worker_id`) ON UPDATE CASCADE;

--
-- Constraints for table `offices`
--
ALTER TABLE `offices`
  ADD CONSTRAINT `fk_offices_city_id` FOREIGN KEY (`k_city_id`) REFERENCES `cities` (`f_city_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_offices_company_id` FOREIGN KEY (`k_company_id`) REFERENCES `companies` (`f_company_id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_role_id` FOREIGN KEY (`k_role_id`) REFERENCES `roles` (`f_role_id`) ON UPDATE CASCADE;

--
-- Constraints for table `workers`
--
ALTER TABLE `workers`
  ADD CONSTRAINT `fk_workers_office_id` FOREIGN KEY (`k_office_id`) REFERENCES `offices` (`f_office_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
